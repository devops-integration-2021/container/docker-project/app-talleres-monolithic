FROM registry.gitlab.com/devops-integration-2021/container/docker-project/tomcat9-openjdk8-centos7

MAINTAINER Warren Roque <wroquem@gmail.com>

COPY app-talleres-monolithic.war /opt/tomcat/webapps
